## LIS4381 A5

### An Nguyen
## Basic server-side validation
>PHP server-side validation, along with prepared statements to help prevent SQL injection.
>Add edit/delete functions to A4.

### Screenshot
![Screenshot](images/screen1.png?raw=true)
![Screenshot](images/screen2.png?raw=true)

### Links to Repo
> 1. [a5 repo](https://bitbucket.org/atn13/lis4381)

### Link to Website
> [Website](http://atn13.com/lis4381/a5/index.php) 