## LIS4381 P2

### An Nguyen
## Basic client-, server-side validation, and RSS feeds.
>PHP server-side validation, along with prepared statements to help prevent SQL injection
>Adds refractoring, using funtion library to A5.

### Screenshot
![Screenshot](images/screen.png?raw=true)

### Links to Repo
> 1. [p2 repo](https://bitbucket.org/atn13/lis4381)

### Link to Website
> [Website](http://atn13.com/lis4381/p2/index.php) 