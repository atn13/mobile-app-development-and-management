<?php
//show error: at least 1 and 4
ini_set('display_errors', 1);
//ini_set('log_errors', 1);

error_reporting(E_ALL);

//get item ID
$pst_id_v = $_POST['pst_id'];

if(empty($pst_id_v))
{
	$error = "Invalid data. check field and try again.";
	include('global/error.php');
}
else
{
	//if valid, delete
	//make sure file is only required once,
	//fail causes error that stops remainder of page from processing
	require_once('global/connection.php');
	
	//pull in function library
	require_once "global/functions.php";
	
	//call function, passing arguments that contain data values using variables
	delete_petstore($pst_id_v);

	header('Location: index.php');
}

/*
require_once('global/connection.php');

//Delete item from database
$query = 
"DELETE FROM petstore
WHERE pst_id = :pst_id_p";

try
{
	$statement = $db->prepare($query);
	$statement->bindParam(':pst_id_p', $pst_id_v);
	$row_count = $statement->execute();
	$statement->closeCursor();
}

catch (PDOException $e)
{
	$error = $e->getMessage();
	echo $error;
}
*/

?>