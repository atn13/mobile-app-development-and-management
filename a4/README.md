## LIS4381 A4

### An Nguyen
## Basic server-side validation
>PHP server-side validation, along with prepared statements to help prevent SQL injection.
>Displays user-enter data, and permits users to add data. (See A5 to update and delete pet store data.)

### Screenshots
![Screenshot](images/screen1.png?raw=true)
![Screenshot](images/screen2.png?raw=true)

### Links to Repo
> 1. [a4 repo](https://bitbucket.org/atn13/lis4381)

### Link to Website
> [Website](http://atn13.com/lis4381/a4/index.php) 
